<?php
/**
 * Displays the next and previous post navigation in single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

$next_post = get_next_post();
$prev_post = get_previous_post();

if ( $next_post || $prev_post ) {

	$pagination_classes = '';

	if ( ! $next_post ) {
		$pagination_classes = ' only-one only-prev';
	} elseif ( ! $prev_post ) {
		$pagination_classes = ' only-one only-next';
	}

	?>

	<nav class="post-nav d-flex justify-content-between <?php echo esc_attr( $pagination_classes ); ?>" aria-label="<?php esc_attr_e( 'Post', 'twentytwenty' ); ?>" role="navigation">

			<?php
			if ( $prev_post ) {
				?>
            <div class="nav-previous">
				<a title="<?php echo wp_kses_post( get_the_title( $prev_post->ID ) ); ?>" rel="prev" href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>">
                    <i class="fas fa-long-arrow-alt-left mr-1" aria-hidden="true"></i>
					<?php echo wp_kses_post( substr(get_the_title( $prev_post->ID ),0,30).'...' ); ?>
				</a>
            </div>
				<?php
			}

			if ( $next_post ) {
				?>
                <div class="nav-next">
                    <a title="<?php echo wp_kses_post( get_the_title( $next_post->ID ) ); ?>" rel="next" href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>">
                        <?php echo wp_kses_post( substr(get_the_title( $next_post->ID ),0,30).'...' ); ?>
                        <i class="fas fa-long-arrow-alt-right ml-1" aria-hidden="true"></i>
                    </a>
                </div>
				<?php
			}
			?>

	</nav><!-- .pagination-single -->

	<?php
}
