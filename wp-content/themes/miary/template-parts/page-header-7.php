<!-- Home -->
<div class="header-intro theme-bg-primary text-white py-5">
    <div class="container">

        <div class="profile-teaser media flex-column flex-md-row">
            <div class="media-body text-md- mr-5">
                <div class="lead"><?php echo esc_html(get_bloginfo( 'description' )); ?></div>
                <h2 class="mt-0 display-4 font-weight-bold"><?php echo esc_html(get_bloginfo( 'blogname' )); ?></h2>
                <div class="bio mb-3"><?php echo get_option('intro_content'); ?></div><!--//bio-->

            </div><!--//media-body-->

            <?php
            // Site title or logo.
            twentytwenty_site_logo();
            ?>
        </div>

    </div>
</div>
