<?php
 $bouton_en_savoir_plus = get_field('label_bouton_en_savoir_plus');
?>
<div class="header-intro theme-bg-primary text-white py-5">
    <div class="container position-relative">
        <div class="media flex-column flex-md-row">
            <div class="media-body align-self-center">
                <h2 class="page-heading mb-2"><?php the_title(); ?></h2>
                <div class="page-heading-tagline mb-3"><?php the_content(); ?></div>
                <ul class="social-list-square list-inline mb-0">

                    <?php
                    $social_network_gitlab = get_option('social_network_gitlab');
                    if ('' !== $social_network_gitlab) :
                    ?>
                    <li class="list-inline-item mb-3 mb-lg-0"><a href="<?= $social_network_gitlab; ?>"><i class="fab fa-gitlab fa-fw text-white"></i></a></li>
                    <?php endif; ?>

                    <?php
                    $social_network_github = get_option('social_network_github');
                    if ('' !== $social_network_github) :
                    ?>
                    <li class="list-inline-item mb-3 mb-lg-0"><a href="<?= $social_network_github; ?>"><i class="fab fa-github-alt fa-fw text-white"></i></a></li>
                    <?php endif; ?>

                    <?php
                    $social_network_medium = get_option('social_network_medium');
                    if ('' !== $social_network_medium) :
                    ?>
                    <li class="list-inline-item mb-3 mb-lg-0"><a href="<?= $social_network_medium; ?>"><i class="fab fa-medium-m fa-fw text-white"></i></a></li>
                    <?php endif; ?>

                    <?php
                    $social_network_devto = get_option('social_network_devto');
                    if ('' !== $social_network_devto) :
                    ?>
                    <li class="list-inline-item mb-3 mb-lg-0"><a href="<?= $social_network_devto; ?>"><i class="fab fa-dev fa-fw text-white"></i></a></li>
                    <?php endif; ?>

                    <?php
                    $social_network_twitter = get_option('social_network_twitter');
                    if ('' !== $social_network_twitter) :
                        ?>
                        <li class="list-inline-item mb-3 mb-lg-0"><a href="<?= $social_network_twitter; ?>"><i class="fab fa-twitter fa-fw text-white"></i></a></li>
                    <?php endif; ?>


                </ul><!--//social-list-->
            </div>
        </div>


    </div><!--//container-->
</div><!--//header-intro-->

<section class="section pt-5">
    <div class="container blog-cards">

        <div class="row">
            <?php
            $args = array(
                'post_type' => 'portofolio',
                'post_status' => 'publish',
                'posts_per_page' => 1,
                'orderby' => 'date',
                'order' => 'DESC',
                'cat' => 'home',
            );

            $loop = new WP_Query( $args );

            while ( $loop->have_posts() ){
                $loop->the_post();
                ?>
                <div class="col-12">
                    <div class="featured-card d-md-table card rounded-0 border-0 shadow-sm mb-5">
                        <div class="featured-card-image card-img-container position-relative d-md-table-cell">
                            <div class="card-img-overlay overlay-mask text-center p-0">
                                <?php
                                $custom_logo_id = get_post_thumbnail_id();
                                $image_src = wp_get_attachment_url( $custom_logo_id);
                                ?>
                                <img src="<?= $image_src; ?>" alt="">
                            </div>
                        </div>

                        <div class="featured-card-body card-body d-md-table-cell  pb-4">
                            <h4 class="card-title mb-2"><a href="<?php the_field('url'); ?>"><?php the_title(); ?></a></h4>
                            <div class="card-text">
                                <div class="excerpt mb-3"><?php the_field('excerpt_project'); ?></div>
                            </div>
                            <div class="card-footer border-0 d-md-none">
                                <ul class="meta list-inline mb-0">
                                    <li class="list-inline-item mr-3"><i class="far fa-clock mr-2"></i><?php the_field('periode'); ?></li>
                                    <li class="list-inline-item"><a href="<?php the_field('url'); ?>"><?php the_field('url'); ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            wp_reset_postdata();
            ?>

            <?php
            $args = array(
                'post_type' => 'portofolio',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'offset' => 2,
                'orderby' => 'date',
                'order' => 'DESC',
                'cat' => 'home',
            );

            $loop = new WP_Query( $args );

            while ( $loop->have_posts() ){
                $loop->the_post();
                ?>
                <div class="col-12 col-md-6 col-lg-4 mb-5">
                    <div class="card rounded-0 border-0 shadow-sm eq-height">
                        <div class="card-img-container position-relative">
                            <?php
                            $custom_logo_id = get_post_thumbnail_id();
                            $image_src = wp_get_attachment_url( $custom_logo_id);
                            ?>
                            <img class="card-img-top rounded-0" src="<?= $image_src; ?>" alt="">
                            <div class="card-img-overlay overlay-mask text-center p-0">
                                <div class="overlay-mask-content text-center w-100 position-absolute">

                                </div>
                            </div>
                        </div>
                        <div class="card-body pb-4">

                            <h4 class="card-title mb-2"><a href="<?php the_field('url'); ?>"><?php the_title(); ?></a></h4>
                            <div class="card-text">

                                <div class="excerpt"><?php the_field('excerpt_project'); ?></div>
                            </div>

                        </div>
                        <div class="card-footer border-0">
                            <ul class="meta list-inline mb-0">
                                <li class="list-inline-item mr-3"><i class="far fa-clock mr-2"></i><?php the_field('periode'); ?></li>
                                <li class="list-inline-item"><a href="<?php the_field('url'); ?>"><?php the_field('url'); ?></a></li>
                            </ul>
                        </div>
                    </div><!--//card-->
                </div>
                <?php
            }
            wp_reset_postdata();
            ?>


        </div>
    </div>
</section>
