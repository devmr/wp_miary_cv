<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

<section class="project-wrapper single-col-max-width py-5 px-4 mx-auto">
    <div class="section-row">
	<?php

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();
    ?>

    <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

        <div class="post-inner <?php echo is_page_template( 'templates/template-full-width.php' ) ? '' : 'thin'; ?> ">

            <div class="entry-content">

                <?php
                    the_content( __( 'Continue reading', 'twentytwenty' ) );
                ?>

            </div><!-- .entry-content -->

        </div><!-- .post-inner -->

        <div class="section-inner">
            <?php
            wp_link_pages(
                array(
                    'before'      => '<nav class="post-nav-links bg-light-background" aria-label="' . esc_attr__( 'Page', 'twentytwenty' ) . '"><span class="label">' . __( 'Pages:', 'twentytwenty' ) . '</span>',
                    'after'       => '</nav>',
                    'link_before' => '<span class="page-number">',
                    'link_after'  => '</span>',
                )
            );

            edit_post_link();

            // Single bottom post meta.
            twentytwenty_the_post_meta( get_the_ID(), 'single-bottom' );

            ?>

        </div><!-- .section-inner -->

    </article><!-- .post -->
    <?php
		}
	}
     ?>
    </div><!-- #site-content -->

    <?php

    if ( is_single() ) {
        get_template_part( 'template-parts/navigation' );
    }
    ?>


</section>


<?php get_footer(); ?>
